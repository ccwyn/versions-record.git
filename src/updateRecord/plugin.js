module.exports = function (babel, options) {
  const {
    types: t
  } = babel;
  const {
    version,
    mode,
    cdnVersions
  } = options;
  return {
    name: "push elements in array",
    visitor: {
      VariableDeclarator(path) {
        const node = path.node;
        if (node.id.name == "service_config") {
          // 获取环境索引
          let envIndex = node.init.properties.findIndex(item => item.key.name === mode)
          // 获取版本号变量索引
          let versionRecordIndex = node.init.properties[envIndex].value.properties.findIndex(item => item.key.name === 'versionRecord')
          if (mode === 'dev') {
            node.init.properties[envIndex].value.properties[versionRecordIndex].value.elements = [t.identifier(`'${version}'`)];
          } else {
            node.init.properties[envIndex].value.properties[versionRecordIndex].value.elements.unshift(t.identifier(`'${version}'`));
            if (cdnVersions) {
              const cdnList = cdnVersions.replace(/\"/g,'').split(',');
              node.init.properties[envIndex].value.properties[versionRecordIndex].value.elements.splice(4)
              node.init.properties[envIndex].value.properties[versionRecordIndex].value.elements.forEach((item,index) => {
                if(cdnList.indexOf(item.value)>-1){
                  node.init.properties[envIndex].value.properties[versionRecordIndex].value.elements.splice(index,1)
                }
              });
              node.init.properties[envIndex].value.properties[versionRecordIndex].value.elements.push(...cdnList.map(item=>t.identifier(`'${item}'`)))
            } 
          }
        }
      },
    },
  };
};