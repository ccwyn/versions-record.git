module.exports = function (babel, options) {
  const { types: t } = babel;
  const { version, field } = options;
  return {
    name: "write in new content",
    visitor: {
      //  捕捉对象属性
      //  t表示的是type,也就是各种属性，要对节点做操作，需要对path做处理，相关api在@babel/traverse里面，市面上几乎没有文档
      ObjectProperty(path) {
        //  遍历所有的对象属性
        const node = path.node;
        //  定位到key为version的对象属性
        if (node.key.value === field) {
          node.value.value = version;
        }
      },
    },
  };
};
