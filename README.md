versions-record
====================

> 生成版本号记录

## 前提

需要 Node 版本在 v8.0 以上
需要 webpack 版本在 v5.0 以上

## 安装

```sh
npm i -D versions-record
```

## 使用方法

支持的配置项:
      
- version package.json版本号
- recordFilePath 输出版本号文件
- mode 版本模式


## Example
#### 作为webpack插件使用
```
const VersionRecord = require('version-record')

// Webpack 的配置
module.exports = {
 plugins: [
    new VersionRecord({
      mode: dev,
      recordFilePath: path.resolve(process.cwd(), `./src/${ModuleFederationConfig.name}_version.js`),
      version: '1.0.0'
    })
 ]
}
```
